module regfile(
    input            clk,               //ʱ��
    input            wen,               //дʹ��
    input  [4:0]     num_a,             //���Ĵ�������a
    input  [4:0]     num_b,             //���Ĵ�������b
    input  [4:0]     num_c,             //д�Ĵ�������
    input  [31:0]    data_c,            //д����
    output [31:0]    data_a,            //��������a
    output [31:0]    data_b             //��������b
`ifdef DIFFTEST_EN
    ,
    output [31:0]    rf_regs_diff [31:0] // for difftest
`endif 
);
    reg    [31:0]    gp_registers[31:0]; //�Ĵ�����
    always @(posedge clk) begin
        if(wen)
            gp_registers[num_c] <= data_c;
    end
    assign data_a = {32{|num_a}} & gp_registers[num_a];
    assign data_b = {32{|num_b}} & gp_registers[num_b];
`ifdef DIFFTEST_EN
    assign rf_regs_diff = gp_registers;
`endif 
endmodule
